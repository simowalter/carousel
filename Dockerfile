FROM arm64v8/nginx:alpine

ADD dist/ /usr/share/nginx/html
COPY nginx-conf/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
