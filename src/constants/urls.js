export const YOGA_URLS = 
[
  "https://images.squarespace-cdn.com/content/v1/5803a411bebafb3a14c2bb8b/1515850550544-4MBMJ6HJ4KHKU23WF5MN/Lola-yoga-meditation-plage-couche-de-soleil.jpg?format=1500w",
  "http://community.thriveglobal.com/wp-content/uploads/2020/05/Depositphotos_176769106_l-2015.jpg",
  "http://blog.hdwallsource.com/wp-content/uploads/2016/06/meditation-silhouette-computer-wallpaper-53139-54861-hd-wallpapers.jpg",
  "https://cdn.newswire.com/files/x/00/28/a06939d5c53cc0e5d63ec8e3d237.jpg",
  "https://1.bp.blogspot.com/-0PHX1RpoE-g/WKxuD-liG5I/AAAAAAAAAe8/OOalDDWgSfwW82l1UuNl6vXxSA3fUfAogCLcB/s1600/Out-Of-Meditation.jpg",
  "https://wallpapercave.com/wp/88kEtxo.jpg",
  "https://i.pinimg.com/originals/52/e5/d1/52e5d1721d3c5515371dbbbe2ac260da.png",
];

  export const HOUSE_URLS = 
  [
       "https://images.unsplash.com/photo-1613490493576-7fde63acd811?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8bHV4dXJ5JTIwaG91c2V8ZW58MHx8MHx8fDA%3D&w=1000&q=80",
       "https://images.pexels.com/photos/1101146/pexels-photo-1101146.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
       "https://images7.alphacoders.com/341/341714.jpg",
       "https://images7.alphacoders.com/341/341475.jpg",
       "https://ww1.prweb.com/prfiles/2015/03/02/12556168/Geneva_Q1_Facade.jpg",
       "https://www.teahub.io/photos/full/205-2052546_beautiful-home-wallpaper-red-brick-house-with-white.jpg",
       "https://hawaiihome.me/wp-content/uploads/2015/04/blog.jpg",
       "https://wallpaperaccess.com/full/1859260.jpg",

];
export const TRAVEL_URLS = 
  [
       "http://www.magazette.fr/wp-content/uploads/monde-du-voyage.jpg",
       "https://evasion-online.com/image-photo/voyage+photos/slider02.jpg",
       "https://media.routard.com/image/00/3/touriste-smartphone-porto.1567003.jpg",
       "https://www.voyage-prive.com/s/images/visual/login/backgrounds/2048x1463-seychelles.jpg",
       "https://i.pinimg.com/originals/b0/a4/57/b0a457a8d3c1b16657d242aad9b1bbb4.jpg",
       "https://i.pinimg.com/originals/1c/cb/c6/1ccbc69c29c1a2e1603ff48684878397.jpg",
       "https://diapogram.com/upload/2019/04/17/20190417205141-d8bedcdd.jpg",
];